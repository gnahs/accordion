/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/accordion.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/accordion.js":
/*!**************************!*\
  !*** ./src/accordion.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }\n\nfunction _nonIterableSpread() { throw new TypeError(\"Invalid attempt to spread non-iterable instance.\\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.\"); }\n\nfunction _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === \"string\") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === \"Object\" && o.constructor) n = o.constructor.name; if (n === \"Map\" || n === \"Set\") return Array.from(o); if (n === \"Arguments\" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }\n\nfunction _iterableToArray(iter) { if (typeof Symbol !== \"undefined\" && iter[Symbol.iterator] != null || iter[\"@@iterator\"] != null) return Array.from(iter); }\n\nfunction _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }\n\nfunction _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }\n\nfunction ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }\n\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\nfunction _typeof(obj) { \"@babel/helpers - typeof\"; if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\n/* eslint-disable class-methods-use-this */\nvar Accordion = /*#__PURE__*/function () {\n  function Accordion(selector) {\n    var settings = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};\n\n    _classCallCheck(this, Accordion);\n\n    this.selector = _typeof(selector) === 'object' ? selector : document.querySelector(selector);\n    this.headers = [];\n    var defaultSettings = {\n      closeOthers: true,\n      orientation: 'vertical'\n    };\n    this.settings = _objectSpread(_objectSpread({}, defaultSettings), settings);\n    this.setOrientation();\n    this.init();\n  }\n\n  _createClass(Accordion, [{\n    key: \"setOrientation\",\n    value: function setOrientation() {\n      if (this.settings.orientation === 'vertical') {\n        this.selector.classList.add('c-accordion--vertical');\n        return;\n      }\n\n      this.selector.classList.add('c-accordion--horizontal');\n    }\n  }, {\n    key: \"init\",\n    value: function init() {\n      var _this = this;\n\n      this.headers = this.selector.querySelectorAll('.c-accordion__head');\n      if (!this.headers.length) return;\n      this.headers.forEach(function (header) {\n        header.addEventListener('click', function (ev) {\n          _this.select(ev);\n        });\n      });\n\n      if (this.settings.orientation === 'horizontal') {\n        var element = this.selector.querySelector('.c-accordion__item');\n        this.openElement(element);\n        var body = this.selector.querySelector('.c-accordion__item.active .c-accordion__body');\n        body.style.maxHeight = \"\".concat(body.scrollHeight, \"px\");\n      }\n    }\n  }, {\n    key: \"select\",\n    value: function select(ev) {\n      var element = ev.target.closest('.c-accordion__item');\n\n      if (element.classList.contains('active')) {\n        this.closeElement(element);\n        return this;\n      }\n\n      if (this.settings.closeOthers) this.closeActive();\n      this.openElement(element);\n      return this;\n    }\n  }, {\n    key: \"openElement\",\n    value: function openElement(element) {\n      element.classList.add('active');\n      var body = element.querySelector('.c-accordion__body');\n\n      if (this.settings.orientation === 'horizontal') {\n        this.calculateMaxHeight(element, body);\n      }\n\n      if (this.settings.orientation === 'vertical') {\n        var parentBody = this.parentBody(body);\n\n        if (parentBody !== null) {\n          parentBody.style.height = \"\".concat(parentBody.scrollHeight + body.scrollHeight, \"px\");\n        }\n\n        body.style.height = \"\".concat(body.scrollHeight, \"px\");\n      }\n\n      this.addCustomEvent('opened');\n    }\n  }, {\n    key: \"closeElement\",\n    value: function closeElement(element) {\n      element.classList.remove('active');\n\n      if (this.settings.orientation === 'vertical') {\n        var body = element.querySelector('.c-accordion__body');\n        var parentBody = this.parentBody(body);\n\n        if (parentBody !== null) {\n          parentBody.style.height = \"\".concat(parentBody.scrollHeight - body.scrollHeight, \"px\");\n        }\n\n        body.style.height = null;\n      }\n\n      return this;\n    }\n  }, {\n    key: \"parentBody\",\n    value: function parentBody(el) {\n      var grandFather = el.parentElement.parentElement;\n\n      if (grandFather.classList.contains('c-accordion__body')) {\n        return grandFather;\n      }\n\n      return null;\n    }\n  }, {\n    key: \"closeActive\",\n    value: function closeActive() {\n      var _this2 = this;\n\n      this.selector.querySelectorAll('.c-accordion__item').forEach(function (item) {\n        if (item.classList.contains('active')) {\n          _this2.closeElement(item);\n        }\n      });\n      return this;\n    }\n  }, {\n    key: \"openAll\",\n    value: function openAll() {\n      var _this3 = this;\n\n      this.selector.querySelectorAll('.c-accordion__item').forEach(function (item) {\n        item.classList.add('active');\n\n        if (_this3.settings.orientation === 'vertical') {\n          var body = item.querySelector('.c-accordion__body');\n          body.style.height = \"\".concat(body.scrollHeight, \"px\");\n        }\n      });\n    }\n  }, {\n    key: \"closeAll\",\n    value: function closeAll() {\n      var _this4 = this;\n\n      this.selector.querySelectorAll('.c-accordion__item').forEach(function (item) {\n        _this4.closeElement(item);\n      });\n    }\n  }, {\n    key: \"addCustomEvent\",\n    value: function addCustomEvent(name) {\n      var event = new CustomEvent(name, {\n        detail: {\n          message: 'Accordion opened'\n        },\n        cancelable: true\n      });\n      this.selector.dispatchEvent(event);\n    }\n  }, {\n    key: \"calculateMaxHeight\",\n    value: function calculateMaxHeight(item, body) {\n      item.addEventListener('transitionend', function (ev) {\n        if (ev.target.offsetParent.className.includes('active')) {\n          // eslint-disable-next-line no-param-reassign\n          body.style.maxHeight = \"\".concat(ev.target.scrollHeight, \"px\");\n        }\n      });\n      return this;\n    }\n  }, {\n    key: \"addAccordionItems\",\n    value: function addAccordionItems(newItems) {\n      var _this5 = this;\n\n      newItems.forEach(function (newItem) {\n        newItem.addEventListener('click', _this5.select.bind(_this5));\n      });\n      this.headers = [].concat(_toConsumableArray(this.headers), _toConsumableArray(newItems));\n    }\n  }]);\n\n  return Accordion;\n}(); // window.GNAHS = {\n//     accordion: Accordion,\n// }\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Accordion);\n\n//# sourceURL=webpack:///./src/accordion.js?");

/***/ })

/******/ });