/* eslint-disable */
jest.setTimeout(2000)

const urlTest = 'http://accordion.test'
// const urlVendor = 'http://accordion.test/public/js/vendor.js'
const urlComponent = 'http://accordion.test/public/js/accordion.js'

const devices = require('puppeteer/DeviceDescriptors')
const iPhonex = devices['iPhone X']

const mainSelector = '.c-accordion-simple'

const customBeforeAll = () => {
    beforeAll (async () => {
        await page.goto(urlTest)
        // await page.addScriptTag({ url: urlVendor })
        await page.addScriptTag({ url: urlComponent })
        await page.evaluate(() => {
            new Accordion('.c-accordion-simple', {
                closeOthers: true,
                orientation: 'vertical'
            })
        })
    })
}


describe ('Init Form handler', () => {

    customBeforeAll()

    it('should create instance', async () => {
        await page.waitForSelector(mainSelector)
        const mainElement = await page.$(mainSelector)
        await expect(page.evaluate(element => element.getAttribute("class"), mainElement)).resolves.toMatch('c-accordion-simple')
    })
})
