/* eslint-disable class-methods-use-this */
class Accordion {
    constructor(selector, settings = {}) {
        this.selector = (typeof selector === 'object') ? selector : document.querySelector(selector)
        this.headers = []
        const defaultSettings = {
            closeOthers: true,
            orientation: 'vertical',
        }

        this.settings = {
            ...defaultSettings,
            ...settings,
        }
        this.setOrientation()
        this.init()
    }

    setOrientation() {
        if (this.settings.orientation === 'vertical') {
            this.selector.classList.add('c-accordion--vertical')
            return
        }

        this.selector.classList.add('c-accordion--horizontal')
    }

    init() {
        this.headers = this.selector.querySelectorAll('.c-accordion__head')
        if (!this.headers.length) return

        this.headers.forEach((header) => {
            header.addEventListener('click', (ev) => {
                this.select(ev)
            })
        })

        if (this.settings.orientation === 'horizontal') {
            const element = this.selector.querySelector('.c-accordion__item')
            this.openElement(element)

            const body = this.selector.querySelector('.c-accordion__item.active .c-accordion__body')
            body.style.maxHeight = `${body.scrollHeight}px`
        }
    }

    select(ev) {
        const element = ev.target.closest('.c-accordion__item')
        if (element.classList.contains('active')) {
            this.closeElement(element)
            return this
        }
        if (this.settings.closeOthers) this.closeActive()
        this.openElement(element)

        return this
    }

    openElement(element) {
        element.classList.add('active')
        const body = element.querySelector('.c-accordion__body')
        if (this.settings.orientation === 'horizontal') {
            this.calculateMaxHeight(element, body)
        }
        if (this.settings.orientation === 'vertical') {
            const parentBody = this.parentBody(body)

            if (parentBody !== null) {
                parentBody.style.height = `${parentBody.scrollHeight + body.scrollHeight}px`
            }

            body.style.height = `${body.scrollHeight}px`
        }

        this.addCustomEvent('opened')
    }

    closeElement(element) {
        element.classList.remove('active')
        if (this.settings.orientation === 'vertical') {
            const body = element.querySelector('.c-accordion__body')

            const parentBody = this.parentBody(body)

            if (parentBody !== null) {
                parentBody.style.height = `${parentBody.scrollHeight - body.scrollHeight}px`
            }

            body.style.height = null
        }
        return this
    }

    parentBody(el) {
        const grandFather = el.parentElement.parentElement
        if (grandFather.classList.contains('c-accordion__body')) {
            return grandFather
        }

        return null
    }

    closeActive() {
        this.selector.querySelectorAll('.c-accordion__item').forEach((item) => {
            if (item.classList.contains('active')) {
                this.closeElement(item)
            }
        })

        return this
    }

    openAll() {
        this.selector.querySelectorAll('.c-accordion__item').forEach((item) => {
            item.classList.add('active')
            if (this.settings.orientation === 'vertical') {
                const body = item.querySelector('.c-accordion__body')
                body.style.height = `${body.scrollHeight}px`
            }
        })
    }

    closeAll() {
        this.selector.querySelectorAll('.c-accordion__item').forEach((item) => {
            this.closeElement(item)
        })
    }

    addCustomEvent(name) {
        const event = new CustomEvent(name, {
            detail: {
                message: 'Accordion opened',
            },
            cancelable: true,
        })
        this.selector.dispatchEvent(event)
    }

    calculateMaxHeight(item, body) {
        item.addEventListener('transitionend', (ev) => {
            if (ev.target.offsetParent.className.includes('active')) {
                // eslint-disable-next-line no-param-reassign
                body.style.maxHeight = `${ev.target.scrollHeight}px`
            }
        })
        return this
    }

    addAccordionItems(newItems) {
        newItems.forEach((newItem) => {
            newItem.addEventListener('click', this.select.bind(this))
        })
        this.headers = [ ...this.headers, ...newItems ]
    }
}
// window.GNAHS = {
//     accordion: Accordion,
// }
export default Accordion
